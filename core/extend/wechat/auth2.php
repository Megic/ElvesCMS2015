<!--返回用户信息到特定页面-->
<?php
require("./lib/ewechat.class.php");
require('../../class/connect.php');
require('../../class/db_sql.php');
require('../../member/class/user.php');
require('../../data/dbcache/MemberLevel.php');
//加载微信配置文件
$configpath="./config/config.php";
if(!empty($_GET['config'])){ 
$configpath="./config/".$_GET['config'].".php";
}
require($configpath);

$weObj = new EWechat($wechat_config['options']); //创建实例对象
//微信授权跳转
if($_GET['type']=='getlogin'){
$url=$weObj->getOauthRedirect('http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?type=loginback&config='.$_GET['config'].'&groupid='.$_GET['groupid'].'&elvefrom='.urlencode($_GET['elvefrom']));
header("Location: ".$url);
exit();
}
//微信授权获取用户信息跳转到指定地址
if($_GET['type']=='loginback') {
    $r = $weObj->getOauthAccessToken();
    $user = $weObj->getOauthUserinfo($r['access_token'], $r['openid']);
    $back = urldecode($_GET['elvefrom']);
    $check = strpos($back, '?');
//如果存在 ?
    if ($check !== false) {
        //如果 ? 后面没有参数，如 http://YITU.org/index.php?
        if (substr($back, $check + 1) == '') {
            //可以直接加上附加参数
            $new_url = $back;
        } else    //如果有参数，如：http://YITU.org/index.php?ID=12
        {
            $new_url = $back . '&';
        }
    } else    //如果不存在 ?
    {
        $new_url = $back . '?';
    }
    $new_url = $new_url . 'openid=' . $user['openid'] . "&username=" . $user['nickname'] . "&sex=" . $user['sex'] . "&province=" . $user['province'] . "&city=" . $user['city'] . "&userpic=" . $user['headimgurl'];
    header("Location: " . $new_url);
    exit();
}
?>