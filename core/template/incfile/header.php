<?php
if(!defined('InElvesCMS'))
{
  exit();
}

//--------------- 界面参数 ---------------

//会员界面附件地址前缀
$memberskinurl=$public_r['newsurl'].'skin/member/images/';

//LOGO图片地址
$logoimgurl=$memberskinurl.'logo.jpg';

//加减号图片地址
$addimgurl=$memberskinurl.'add.gif';
$noaddimgurl=$memberskinurl.'noadd.gif';

//上下横线背景色
$bgcolor_line='#4FB4DE';

//其它色调可修改CSS部分

//--------------- 界面参数 ---------------


//识别并显示当前菜单
function ElveShowThisMemberMenu(){
  global $memberskinurl,$noaddimgurl;
  $selffile=eReturnSelfPage(0);
  if(stristr($selffile,'/member/msg'))
  {
    $menuname='menumsg';
  }
  elseif(stristr($selffile,'core/DoInfo'))
  {
    $menuname='menuinfo';
  }
  elseif(stristr($selffile,'/member/mspace'))
  {
    $menuname='menuspace';
  }
  elseif(stristr($selffile,'core/ShopSys'))
  {
    $menuname='menushop';
  }
  elseif(stristr($selffile,'core/payapi')||stristr($selffile,'/member/buygroup')||stristr($selffile,'/member/card')||stristr($selffile,'/member/buybak')||stristr($selffile,'/member/downbak'))
  {
    $menuname='menupay';
  }
  else
  {
    $menuname='menumember';
  }
  echo'<script>turnit(do'.$menuname.',"'.$menuname.'img");</script>';
  ?>
  <script>
  do<?=$menuname?>.style.display="";
  document.images.<?=$menuname?>img.src="<?=$noaddimgurl?>";
  </script>
  <?php
}

//网页标题
$thispagetitle=$public_diyr['pagetitle']?$public_diyr['pagetitle']:'会员中心';
//会员信息
$tmgetuserid=(int)getcvar('mluserid');  //用户ID
$tmgetusername=RepPostVar(getcvar('mlusername')); //用户名
$tmgetgroupid=(int)getcvar('mlgroupid');  //用户组ID
$tmgetgroupname='游客';
//会员组名称
if($tmgetgroupid)
{
  $tmgetgroupname=$level_r[$tmgetgroupid]['groupname'];
  if(!$tmgetgroupname)
  {
    include_once(ELVE_PATH.'core/data/dbcache/MemberLevel.php');
    $tmgetgroupname=$level_r[$tmgetgroupid]['groupname'];
  }
}

//模型
$tgetmid=(int)$_GET['mid'];
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>会员中心</title>
    <meta name="renderer" content="webkit">
    <script>(function(){function a(){return/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)?!0:/(Android)/i.test(navigator.userAgent)?!0:!1}var d=document.getElementsByTagName("html")[0];var e;if(a()){window.isMobile=1;e="MC"}else{window.isMobile=0;e="PC"}d.className=d.className?d.className+" "+e:e;var b=d.className;function c(f){var h="";var g=document.documentElement.clientWidth;1600>g&&(h+=" lt1600");1400>g&&(h+=" lt1400");1200>g&&(h+=" lt1200");1000>g&&(h+=" lt1000");960>g&&(h+=" lt960");640>g&&(h+=" lt640");500>g&&(h+=" lt500");400>g&&(h+=" lt400");1600<g&&(h+=" gt1600");1400<g&&(h+=" gt1400");1200<g&&(h+=" gt1200");1000<g&&(h+=" gt1000");960<g&&(h+=" gt960");640<g&&(h+=" gt640");500<g&&(h+=" gt500");400<g&&(h+=" gt400");f.className=b+h}c(d);window.onresize=function(f){c(d)}})();</script>
    <!--让IE9以下浏览器支持html5标签 -->
    <!--if lt IE 9]><script src="http://cdn.bootcss.com/html5shiv/r29/html5.min.js"></script><![endif]-->
    <link rel="stylesheet" href="<?=$public_r['newsurl']?>skin/member/css/base.min.css">
    <link rel="stylesheet" href="<?=$public_r['newsurl']?>skin/member/css/style.css">
    <style>.m-color{ border-color: #358f09;}</style>
<script src="<?=$public_r['newsurl']?>skin/member/js/jquery.min.js"></script>
<script src="<?=$public_r['newsurl']?>skin/member/js/layer/layer.js" ></script>
<script src="<?=$public_r['newsurl']?>skin/member/js/avalon.min.js"></script>
<script>
    var app=avalon.define({
        $id:'app',
        isMobile:isMobile
    });
   $(function(){
       var loading = $('#loading');
       loading.addClass('hide');
       setTimeout(function () {
           loading.remove();
       }, 300);
var lock=false;
$('.show-menu').add('.mj-mcmenu-close').click(function(){
if(!lock){$('.mj-mcmenu-body').append($('.menu').html());lock=1;}
$('.mj-mcmenu').toggleClass('on');
});

   })
</script>
</head>
<body ms-controller="app">
<!--loading.begin-->
<div id="loading">
    <div id="loading-box"> <p>页面加载中...</p></div>
</div>
<!--loading.end-->
<div class="mj-mcmenu">
      <button class="mj-mcmenu-close">X</button>
    <div class="mj-mcmenu-box"> <div class="mj-mcmenu-body"></div></div>
</div>


<header class="m-color">
<div class="w-main f-bc f-pr">
<button class="show-menu pc-hide"><i class="f-icon">&#xe614;</i></button>
    <img src="<?=$public_r['newsurl']?>skin/images/logo.jpg" class="logo">
    <nav class="f-pa mc-hide">
     <?php
  if($tmgetuserid)  //已登录
  {
  ?>
        <span>您好，<?=$tmgetusername?>！</span>
        <span class="a-nav">
            <a href="<?=$public_r['newsurl']?>"><i class="f-icon">&#xe600;</i>首页</a><a href="<?=$public_r['newsurl']?>core/member/cp/"><i class="f-icon">&#xe607;</i>会员中心</a><a href="<?=$public_r['newsurl']?>core/member/doaction.php?melve=exit" onclick="return confirm('确认要退出?');"><i class="f-icon">&#xe617;</i>退出登录</a>
        </span>
    <?php  }
  else  //游客
  {
  ?>
<span>您好，游客！</span>
        <span class="a-nav">
            <a href="<?=$public_r['newsurl']?>"><i class="f-icon">&#xe600;</i>首页</a><a href="<?=$public_r['newsurl']?>core/member/login/">登录</a><a href="<?=$public_r['newsurl']?>core/member/register/">注册</a>
        </span>
 <?php } ?>
    </nav>
</div>
</header>
<div id="main">
    <div class="w-main f-bc f-cb f-pt20 f-pb20">
<?php
  if($tmgetuserid)  //已登录
  {
  ?>
        <div class="g-w20 g-fl f-bgc mc-hide menu">
<div class="menu-list m-color pc-hide"><i class="f-icon">&#xe600;</i>常用</div>
          <ul class="menu-ul pc-hide">
<li><a href="<?=$public_r['newsurl']?>">· 站点首页</a></li>
<li><a href="<?=$public_r['newsurl']?>core/member/cp/">· 中心首页</a></li>
<li><a href="<?=$public_r['newsurl']?>core/member/doaction.php?melve=exit" onclick="return confirm('确认要退出?');">· 退出登录</a></li>
</ul>
              <div class="menu-list m-color"><i class="f-icon">&#xe613;</i>账户信息</div>
            <ul class="menu-ul">
                <li><a href="<?=$public_r['newsurl']?>core/member/EditInfo/">· 修改个人资料</a></li>
<li><a href="<?=$public_r['newsurl']?>core/member/EditInfo/EditSafeInfo.php">· 修改登录信息</a></li>
   <!--
<li><a href="<?=$public_r['newsurl']?>core/member/my/">· 帐号状态</a></li>
<li><a href="<?=$public_r['newsurl']?>core/member/fava/">· 收藏夹</a></li>
<li><a href="<?=$public_r['newsurl']?>core/member/friend/">· 好友列表</a></li>
<li><a href="<?=$public_r['newsurl']?>core/memberconnect/ListBind.php">· 第三方登录</a></li>
-->
            </ul>
            
            <div class="menu-list m-color"><i class="f-icon">&#xe61d;</i>分享投稿</div>
            <ul class="menu-ul">
<?php
      //输出可管理的模型
      $tmodsql=$elves->query("select mid,qmname from {$dbtbpre}melvemod where usemod=0 and showmod=0 and qenter<>'' order by myorder,mid");
                        while($tmodr=$elves->fetch($tmodsql))
                        {
                        $fontb="";
                        if($tmodr['mid']==$tgetmid){$fontb="on";}
                        ?>
                <li><a href="<?=$public_r['newsurl']?>core/DoInfo/ListInfo.php?mid=<?=$tmodr['mid']?>" class="<?=$fontb?>">· <?=$tmodr[qmname]?></a></li>
 <?php } ?>
            </ul>
            <!--
<div class="menu-list m-color"><i class="f-icon">&#xe60a;</i>站内消息</div>
            <ul class="menu-ul">
                <li><a href="<?=$public_r['newsurl']?>core/member/msg/AddMsg/?melve=AddMsg">· 发送消息</a></li>
 <li><a href="<?=$public_r['newsurl']?>core/member/msg/">· 消息列表</a></li>
            </ul>
<div class="menu-list m-color"><i class="f-icon">&#xe613;</i>个人空间</div>
            <ul class="menu-ul">
                <li><a href="<?=$public_r['newsurl']?>core/space/?userid=<?=$tmgetuserid?>">· 预览空间</a></li>
<li><a href="<?=$public_r['newsurl']?>core/member/mspace/SetSpace.php">· 设置空间</a></li>
<li><a href="<?=$public_r['newsurl']?>core/member/mspace/ChangeStyle.php">· 选择模板</a></li>
<li><a href="<?=$public_r['newsurl']?>core/member/mspace/gbook.php">· 管理留言</a></li>
<li><a href="<?=$public_r['newsurl']?>core/member/mspace/feedback.php">· 管理反馈</a></li>
            </ul>
            <div class="menu-list m-color"><i class="f-icon">&#xe613;</i>财务</div>
            <ul class="menu-ul">
                <li><a href="<?=$public_r['newsurl']?>core/payapi/">· 在线支付</a></li>
<li><a href="<?=$public_r['newsurl']?>core/member/buygroup/">· 在线充值</a></li>
<li><a href="<?=$public_r['newsurl']?>core/member/card/">· 点卡充值</a></li>
<li><a href="<?=$public_r['newsurl']?>core/member/buybak/">· 点卡充值记录</a></li>
<li><a href="<?=$public_r['newsurl']?>core/member/downbak/">· 下载消费记录</a></li>
            </ul>
            <div class="menu-list m-color"><i class="f-icon">&#xe613;</i>商城</div>
            <ul class="menu-ul">
                <li><a href="<?=$public_r['newsurl']?>core/ShopSys/ListDd/">· 我的订单</a></li>
 <li><a href="#elve" onclick="window.open('<?=$public_r['newsurl']?>core/ShopSys/buycar/','','width=680,height=500,scrollbars=yes,resizable=yes');">· 我的购物车</a></li>
 <li><a href="<?=$public_r['newsurl']?>core/ShopSys/address/ListAddress.php">· 管理配送地址</a></li>
            </ul>
-->
        </div>
 <?php } ?>
        <div class="<?=$tmgetuserid?'g-w75 f-bgc':'g-w';?> g-fr g-mw">
