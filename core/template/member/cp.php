<?php
if(!defined('InElvesCMS'))
{
	exit();
}
?>
<?php
$public_diyr['pagetitle']='会员中心';
$url="<a href=../../../>首页</a>&nbsp;>&nbsp;<a href=../cp/>会员中心</a>";
require(ELVE_PATH.'core/template/incfile/header.php');
?>
 <div class="page-title f-pr">
                <h1 class="page-title-h1 m-color">会员中心</h1> <div class="f-pa title-nav"><a href="javascript:history.go(-1);" class="f-btn f-btn-sm">返回</a></div>
            </div>
 <div class="f-m20">
               <div class="f-cb">
                   <div class="g-w20 g-fl g-mw">
                       <div class="f-tac">
                           <img src="<?=$userpic?>"  width="100px">
                           <p> <?=$user[username]?></p>
                       </div>
                   </div>
                   <div class="g-w80 g-fl g-mw">
                      <div class="f-pl15">
                          <table width="100%" border="0" align="center" cellpadding="3" cellspacing="1" class="tableborder">
                              <tbody><tr bgcolor="#FFFFFF">
                                  <td class="table-left" height="25">用户ID:</td>
                                  <td height="25"><?=$user[userid]?>  </td>
                              </tr>
                              <tr bgcolor="#FFFFFF">
                                  <td class="table-left" height="25">用户名:</td>
                                  <td height="25"><?=$user[username]?>                                  </td>
                              </tr>
                              <tr bgcolor="#FFFFFF">
                                  <td class="table-left" height="25">注册时间:</td>
                                  <td  height="25"><?=$registertime?>             </td>
                              </tr>
                              <tr bgcolor="#FFFFFF">
                                  <td class="table-left" height="25">会员等级:</td>
                                  <td height="25">   <?=$level_r[$r[groupid]][groupname]?>        </td>
                              </tr>
                              <tr bgcolor="#FFFFFF">
                                  <td class="table-left" height="25">剩余有效期:</td>
                                  <td height="25"><?=$userdate?> 天 </td>
                              </tr>
                              <!-- <tr bgcolor="#FFFFFF">
                                  <td class="table-left" height="25">剩余点数:</td>
                                  <td height="25"><?=$r[userfen]?> 点</td>
                              </tr>
                              <tr bgcolor="#FFFFFF">
                                  <td class="table-left" height="25">帐户余额:</td>
                                  <td height="25"><?=$r[money]?>  元 </td>
                              </tr>
-->
                              <tr bgcolor="#FFFFFF">
                                  <td class="table-left" height="25">新消息:</td>
                                  <td height="25">   <?=$havemsg?> </td>
                              </tr>
                              </tbody></table>
                      </div>
                   </div>
               </div>
            </div>
<?php
require(ELVE_PATH.'core/template/incfile/footer.php');
?>