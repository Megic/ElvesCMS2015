<?php
if(!defined('InElvesCMS'))
{
	exit();
}
?>
<?php
$public_diyr['pagetitle']='注册会员';
$url="<a href=../../../>首页</a>&nbsp;>&nbsp;<a href=../cp/>会员中心</a>&nbsp;>&nbsp;选择注册会员类型";
require(ELVE_PATH.'core/template/incfile/header.php');
?>

<table  border="0" align="center" cellpadding="3"  cellspacing="1" class="tableborder f-bgc g-w50 g-mw">
  <form name="ChRegForm" method="GET" action="index.php">
  <input name="tobind" type="hidden" id="tobind" value="<?=$tobind?>">
    <tr class="header"> 
      <td height="25"><div align="center">选择注册会员类型<?=$tobind?' (绑定账号)':''?></div></td>
    </tr>
    <tr bgcolor="#FFFFFF"> 
      <td width="70%" height="25">
<select name="groupid" class="g-w">
<?php
		while($r=$elves->fetch($sql))
		{
			$checked='';
			if($r[groupid]==eReturnMemberDefGroupid())
			{
				$checked=' checked';
			}
		?>
    <option value="<?=$r[groupid]?>" <?=$checked?>><?=$r[groupname]?></option>
<?php
		}
		?>
</select>

</td>
    </tr>
    <tr bgcolor="#FFFFFF"> 
      <td height="25"> <button class="f-btn f-btn-primary" type="submit">下一步</button></td>
    </tr>
  </form>
</table>
<br>
<?php
require(ELVE_PATH.'core/template/incfile/footer.php');
?>