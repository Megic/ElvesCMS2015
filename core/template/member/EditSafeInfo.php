<?php
if(!defined('InElvesCMS'))
{
	exit();
}
?>
<?php
$public_diyr['pagetitle']='修改资料';
$url="<a href=../../../>首页</a>&nbsp;>&nbsp;<a href=../cp/>会员中心</a>&nbsp;>&nbsp;修改安全信息";
require(ELVE_PATH.'core/template/incfile/header.php');
?>

<div class="page-title f-pr">
                <h1 class="page-title-h1 m-color">修改登录信息</h1> <div class="f-pa title-nav"><a href="javascript:history.go(-1);" class="f-btn f-btn-sm">返回</a></div>
            </div>
 <div class="f-m10">
<table width='100%' border='0' align='center' cellpadding='3' cellspacing='1' class="tableborder">
  <form name=userinfoform method=post enctype="multipart/form-data" action=../doaction.php>
    <input type=hidden name=melve value=EditSafeInfo>
    <tr> 
      <td width='21%' height="25" bgcolor="#FFFFFF"> <div align='left'>用户名 </div></td>
      <td width='79%' height="25" bgcolor="#FFFFFF"> 
        <?=$user[username]?>
      </td>
    </tr>
    <tr> 
      <td height="25" bgcolor="#FFFFFF"> <div align='left'>原密码</div></td>
      <td height="25" bgcolor="#FFFFFF"> <input name='oldpassword' type='password' id='oldpassword' size="38" maxlength='20'>
        (修改密码或邮箱时需要密码验证)</td>
    </tr>
    <tr> 
      <td height="25" bgcolor="#FFFFFF"> <div align='left'>新密码</div></td>
      <td height="25" bgcolor="#FFFFFF"> <input name='password' type='password' id='password' size="38" maxlength='20'>
        (不想修改请留空)</td>
    </tr>
    <tr> 
      <td height="25" bgcolor="#FFFFFF"> <div align='left'>确认新密码</div></td>
      <td height="25" bgcolor="#FFFFFF"> <input name='repassword' type='password' id='repassword' size="38" maxlength='20'>
        (不想修改请留空)</td>
    </tr>
    <tr> 
      <td height="25" bgcolor="#FFFFFF"> <div align='left'>邮箱</div></td>
      <td height="25" bgcolor="#FFFFFF"> <input name='email' type='text' id='email' value='<?=$user[email]?>' size="38" maxlength='50'>
      </td>
    </tr>
    <tr> 
            <td colspan="2">
<div class="f-tac f-p10">
    <button class="f-btn f-btn-primary" type="submit"><i class="f-icon">&#xe60f;</i>提交</button>
</div>
      </td>
    </tr>
  </form>
</table>
</div>
<?php
require(ELVE_PATH.'core/template/incfile/footer.php');
?>