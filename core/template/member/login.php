<?php
if(!defined('InElvesCMS'))
{
	exit();
}
?>
<?php
$public_diyr['pagetitle']='会员登录';
$url="<a href=../../../>首页</a>&nbsp;>&nbsp;<a href=../cp/>会员中心</a>&nbsp;>&nbsp;会员登录";
require(ELVE_PATH.'core/template/incfile/header.php');
?>
<br>
  <table border="0" align="center" cellpadding="3" cellspacing="1" class="tableborder f-bgc">
  <form name="form1" method="post" action="../doaction.php">
    <input type=hidden name=elvefrom value="<?=ehtmlspecialchars($_GET['from'])?>">
    <input type=hidden name=melve value=login>
	<input name="tobind" type="hidden" id="tobind" value="<?=$tobind?>">
    <tr class="header"> 
      <td height="25" colspan="2"><div align="center">会员登录<?=$tobind?' (绑定账号)':''?></div></td>
    </tr>
    <tr bgcolor="#FFFFFF"> 
      <td width="23%" height="25">手机/邮箱：</td>
      <td width="77%" height="25"><input name="username" type="text" id="username" >
	  	<?php
		if($public_r['regacttype']==1)
		{
		?>
        &nbsp;&nbsp;<a href="../register/regsend.php" target="_blank">帐号未激活？</a>
		<?php
		}
		?>
		</td>
    </tr>
    <tr bgcolor="#FFFFFF"> 
      <td height="25">密码：</td>
      <td height="25"><input name="password" type="password" id="password"></td>
    </tr>
	 <tr bgcolor="#FFFFFF">
      <td height="25">保存时间：</td>
      <td height="25">
	  <select name="lifetime" class="form-control" id="lifetime"> <option value="2592000">一个月</option> <option value="0">不保存</option> <option value="3600">一小时</option> <option value="86400">一天</option> <option value="315360000">永久</option> </select>
<a href="../GetPassword/"  class="tips" target="_blank">忘记密码？</a>    
</td>

</tr>
    <?php
	if($public_r['loginkey_ok'])
	{
	?>
    <tr bgcolor="#FFFFFF"> 
      <td height="25">验证码：</td>
      <td height="25"><input name="key" type="text" id="key" size="6">
        <img src="../../ShowKey/?v=login" name="loginKeyImg" id="loginKeyImg" onclick="loginKeyImg.src='../../ShowKey/?v=login&t='+Math.random()" title="看不清楚,点击刷新"></td>
    </tr>
    <?php
	}	
	?>
    <tr bgcolor="#FFFFFF"> 
 <td colspan="2">
<div class="f-tac f-p10">
    <button class="f-btn f-btn-primary" type="submit">登录</button>
  <button class="f-btn" type="button"  onclick="parent.location.href='../register/<?=$tobind?'?tobind=1':''?>';">注册</button>

</div>
   </td>
</tr>
	</form>
  </table>
<br>
<?php
require(ELVE_PATH.'core/template/incfile/footer.php');
?>