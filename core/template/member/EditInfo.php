<?php
if(!defined('InElvesCMS'))
{
	exit();
}
?>
<?php
$public_diyr['pagetitle']='修改资料';
$url="<a href=../../../>首页</a>&nbsp;>&nbsp;<a href=../cp/>会员中心</a>&nbsp;>&nbsp;修改资料";
require(ELVE_PATH.'core/template/incfile/header.php');
?>

<div class="page-title f-pr">
                <h1 class="page-title-h1 m-color">修改基本资料</h1> <div class="f-pa title-nav"><a href="javascript:history.go(-1);" class="f-btn f-btn-sm">返回</a></div>
            </div>
 <div class="f-m10 tableborder">
  <form name=userinfoform method=post enctype="multipart/form-data" action=../doaction.php>
    <input type=hidden name=melve value=EditInfo>
   
        <?php
	@include($formfile);
	?>
      
<div class="f-tac f-p10">
    <button class="f-btn f-btn-primary" type="submit"><i class="f-icon">&#xe60f;</i>提交</button>
</div>
      
  </form>
 </div>
<?php
require(ELVE_PATH.'core/template/incfile/footer.php');
?>