
//showsmallpics
function yelveShowSmallPics(){
	var str='';
	var selectpages='';
	var listpages='';
	var i;
	var cname='';
	var lname='';
	var sname='';
	for(i=0;i<elvepicnum;i++)
	{
		cname='';
		lname='';
		sname='';
		if(i==0)
		{
			cname=' class="espiccss"';
			lname=' class="epiclpcss"';
			sname=' selected';
		}
		str+='<td bgcolor="#cccccc" align="center" id="espicid'+i+'"'+cname+'><a href="#elvescms" onclick="elveShowPic('+i+');"><img src="'+elvepicr[i][0]+'" width="'+epicswidth+'" height="'+epicsheight+'" border="0"></a><br>'+(i+1)+'/'+elvepicnum+'</td>';

		selectpages+='<option value="'+i+'"'+sname+'>第 '+(i+1)+' 页</option>';

		listpages+='<a href="#elvescms" id="epiclpid'+i+'" onclick="elveShowPic('+i+');"'+lname+'>'+(i+1)+'</a> ';
	}
	if(eopensmallpics==1)
	{
		document.getElementById("elvesmallpicsid").innerHTML='<table><tr>'+str+'</tr></table>';
	}
	if(eopenselectpage==1)
	{
		document.getElementById("elveselectpagesid").innerHTML='<select name="tothepicpage" id="tothepicpage" onchange="elveShowPic(this.options[this.selectedIndex].value);">'+selectpages+'</select>';
	}
	if(eopenlistpage==1)
	{
		document.getElementById("elvelistpagesid").innerHTML=listpages;
	}
	document.getElementById("ethispage").value=0;
}

//showpic
function elveShowPic(page){
	var thispage=document.getElementById("ethispage").value;
	var sdivwidth=document.getElementById("elvesmallpicsid").offsetWidth;
	if(document.getElementById("elvesmallpicid")!=null)
	{
		document.getElementById("elvesmallpicid").src=elvepicr[page][0];
	}
	document.getElementById("elvebigpicid").src=elvepicr[page][1];
	if(document.getElementById("elvepicnameid")!=null)
	{
		document.getElementById("elvepicnameid").innerHTML=elvepicr[page][2];
	}
	//document.getElementById("elvesmallpicsid").scrollLeft+=120;
	if(page>thispage)
	{
		if(epicswidth*(page+1)>sdivwidth-epicswidth)
		{
			document.getElementById("elvesmallpicsid").scrollLeft+=epicswidth;
		}
	}
	else
	{
		if(epicswidth*(page-1)<document.getElementById("elvesmallpicsid").scrollLeft)
		{
			document.getElementById("elvesmallpicsid").scrollLeft-=epicswidth;
		}
	}
	document.getElementById("ethispage").value=page;
	
	if(eopensmallpics==1)
	{
		document.getElementById("espicid"+page).className='espiccss';
		document.getElementById("espicid"+thispage).className='';
	}
	if(eopenlistpage==1)
	{
		document.getElementById("epiclpid"+page).className='epiclpcss';
		document.getElementById("epiclpid"+thispage).className='';
	}
	if(eopenselectpage==1)
	{
		document.getElementById("tothepicpage").options[page].selected=true;
	}
}

//shownextpic
function elveShowNextPic(){
	var thispage=parseInt(document.getElementById("ethispage").value);
	if(thispage+1>=elvepicnum)
	{
		return false;
	}
	elveShowPic(thispage+1);
}

//showprepic
function elveShowPrePic(){
	var thispage=parseInt(document.getElementById("ethispage").value);
	if(thispage<=0)
	{
		return false;
	}
	elveShowPic(thispage-1);
}

//showtruepic
function elveShowTruePic(){
	var thispage=parseInt(document.getElementById("ethispage").value);
	window.open(elvepicr[thispage][1]);
}

//movespic
function elveMoveSmallPics(scrollwidth){
	document.getElementById("elvesmallpicsid").scrollLeft+=scrollwidth;
}

//autoshowpic
function elvePicAutoPlay(){
	var sec=parseInt(document.getElementById("autoplaysec").value);
	var i;
	for(i=1;i<=sec;i++)
	{
		if(document.getElementById("autoplaystop").value==0)
		{
			window.setTimeout("elvePicAutoPlayDo("+i+","+sec+")",i*1000);
		}
		else
		{
			break;
		}
	}
}

function elvePicAutoPlayDo(num,sec){
	var t;
	if(document.getElementById("autoplaystop").value==1)
	{
		return "";
	}
	if(num==sec) 
	{
		t=elveShowNextPic();
		if(t==false)
		{
			elveShowPic(0);
		}
		elvePicAutoPlay();
	}
}


document.onkeydown=function(event){
	var e = event || window.event || arguments.callee.caller.arguments[0];
	if(e && e.keyCode==37)
	{
		elveShowPrePic();
	}
	if(e && e.keyCode==39)
	{
		elveShowNextPic();
	}
}; 
