<?php
define('ElvesCMSAdmin','1');
require("../../class/connect.php");
require("../../class/db_sql.php");
require("../../class/functions.php");
$link=db_connect();
$elves=new mysqlquery();
$editor=1;
//验证用户
$lur=is_login();
$logininid=$lur['userid'];
$loginin=$lur['username'];
$loginrnd=$lur['rnd'];
$loginlevel=$lur['groupid'];
$loginadminstyleid=$lur['adminstyleid'];
//ehash
$elve_hashur=hReturnElveHashStrAll();

$getvar=ehtmlspecialchars($_GET['getvar']);
$returnvar=ehtmlspecialchars($_GET['returnvar']);
$fun=ehtmlspecialchars($_GET['fun']);
$notfullpage=ehtmlspecialchars($_GET['notfullpage']);
db_close();
$elves=null;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title>在线编辑模板</title>
<link href="../adminstyle/<?=$loginadminstyleid?>/adminstyle.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../elveeditor/teditor/codemirror.css"/>
    <link rel="stylesheet" href="../elveeditor/teditor/monokai.css"/>
<script type="text/javascript" src="../elveeditor/teditor/codemirror.js"></script>
    <script type="text/javascript" src="../elveeditor/teditor/css.js"></script>
    <script type="text/javascript" src="../elveeditor/teditor/xml.js"></script>
    <script type="text/javascript" src="../elveeditor/teditor/javascript.js"></script>
    <script type="text/javascript" src="../elveeditor/teditor/htmlmixed.js"></script>
    <script type="text/javascript" src="../elveeditor/teditor/sublime.js"></script>
    <script type="text/javascript" src="../elveeditor/teditor/emmet.js"></script>
    <style>
        #htmlBox{
            width: 100%;min-height: 500px;
        }
        .CodeMirror{
            min-height: 460px;
            width: 100%;}
    </style>
</head>

<body leftmargin="0" topmargin="0">
<table width="100%" border="0" cellspacing="1" cellpadding="3" class="tableborder">
  <form name="edittemp" method="post" action="../melve.php" onsubmit="return SaveTemp()">
  <?=$elve_hashur['eform']?>
    <tr class="header"> 
      <td width="82%">在线编辑模板</td>
      <td width="18%"> 
        <div align="right"> 

        </div></td>
    </tr>
    <tr> 
      <td colspan="2" bgcolor="#FFFFFF"> <div align="center"> 
          <input type="button" name="Submit" value=" 保存内容 " onclick="return SaveTemp()">
        </div></td>
    </tr>
    <tr> 
      <td colspan="2" bgcolor="#FFFFFF"><div id="mainBox">
              <textarea id="htmlBox"></textarea>
        </div></td>
    </tr>
    <tr> 
      <td colspan="2" bgcolor="#FFFFFF"> <div align="center"> 
          <input type="button" name="Submit2" value=" 保存内容 " onclick="return SaveTemp()">
        </div></td>
    </tr>
    <tr> 
      <td colspan="2" bgcolor="#FFFFFF"> &nbsp;[<a href="#elve" onclick="window.open('EnewsBq.php<?=$elve_hashur['whehref']?>','','width=600,height=500,scrollbars=yes,resizable=yes');">查看模板标签语法</a>] 
        &nbsp;&nbsp;[<a href="#elve" onclick="window.open('../ListClass.php<?=$elve_hashur['whehref']?>','','width=800,height=600,scrollbars=yes,resizable=yes');">查看JS调用地址</a>] 
        &nbsp;&nbsp;[<a href="#elve" onclick="window.open('ListTempvar.php<?=$elve_hashur['whehref']?>','','width=800,height=600,scrollbars=yes,resizable=yes');">查看公共模板变量</a>] 
        &nbsp;&nbsp;[<a href="#elve" onclick="window.open('ListBqtemp.php<?=$elve_hashur['whehref']?>','','width=800,height=600,scrollbars=yes,resizable=yes');">查看标签模板</a>]</td>
    </tr>
  </form>
</table>
<script type="text/javascript">
    var box=document.getElementById('mainBox');
    box.style.width=window.width;
    var htmlBox = document.getElementById('htmlBox');
    var htmlCode = CodeMirror.fromTextArea(htmlBox, {
        lineNumbers: true,
        mode: "text/html",
        keyMap: "sublime"
    });
    htmlCode.setValue(<?=$getvar?>);
    function GetContents()
    {
        // Get the editor contents in XHTML.
        return htmlCode.getValue();		// "true" means you want it formatted.
    }
    function SaveTemp(){
        var isok=confirm('确认要保存?');
        if(isok)
        {
            <?=$returnvar?>=GetContents();
            window.close();
        }
        return false;
    }
</script>
</body>
</html>
