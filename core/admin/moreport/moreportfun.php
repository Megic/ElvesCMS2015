<?php

//更新数据库缓存
function Moreport_ChangeCacheAll($add,$userid,$username){
	global $elves,$dbtbpre,$public_r,$elve_config,$fun_r;
	$addcs=Moreport_ChangeAddCs($add);
	if(!$add['docache'])
	{
		echo"<script>self.location.href='ListMoreport.php?melve=MoreportUpdateClassfileAll&start=0".$addcs.hReturnElveHashStrHref(0)."';</script>";
		exit();
	}
	$start=(int)$add['start'];
	$num=1;
	$b=0;
	$sql=$elves->query("select * from {$dbtbpre}melvemoreport where pid>$start order by pid limit ".$num);
	while($r=$elves->fetch($sql))
	{
		$b=1;
		$new_start=$r['pid'];
		if($r['pid']==1)
		{
			continue;
		}
		if(!$r['ppath']||!file_exists($r['ppath'].'core/config/config.php'))
		{
			continue;
		}
		define('ELVE_SELFPATH',$r['ppath']);
		Moreport_ChangeData($r,0);
	}
	if(empty($b))
	{
		echo $fun_r[MoreportChangeCacheSuccess]."<script>self.location.href='ListMoreport.php?melve=MoreportUpdateClassfileAll&start=0".$addcs.hReturnElveHashStrHref(0)."';</script>";
		exit();
	}
	echo"<meta http-equiv=\"refresh\" content=\"".$public_r['realltime'].";url=ListMoreport.php?melve=MoreportChangeCacheAll&start=$new_start".$addcs.hReturnElveHashStrHref(0)."\">".$fun_r[OneMoreportChangeCacheSuccess]."(ID:<font color=red><b>".$new_start."</b></font>)";
	exit();
}

//更新栏目缓存文件
function Moreport_UpdateClassfileAll($add,$userid,$username){
	global $elves,$dbtbpre,$public_r,$elve_config,$fun_r;
	$addcs=Moreport_ChangeAddCs($add);
	if(!$add['doclassfile'])
	{
		echo"<script>self.location.href='ListMoreport.php?melve=MoreportReDtPageAll&start=0".$addcs.hReturnElveHashStrHref(0)."';</script>";
		exit();
	}
	$start=(int)$add['start'];
	$num=1;
	$b=0;
	$sql=$elves->query("select * from {$dbtbpre}melvemoreport where pid>$start order by pid limit ".$num);
	while($r=$elves->fetch($sql))
	{
		$b=1;
		$new_start=$r['pid'];
		if($r['pid']==1)
		{
			continue;
		}
		if(!$r['ppath']||!file_exists($r['ppath'].'core/config/config.php'))
		{
			continue;
		}
		define('ELVE_SELFPATH',$r['ppath']);
		Moreport_ChangeData($r,3);
	}
	if(empty($b))
	{
		echo $fun_r[MoreportUpdateClassfileSuccess]."<script>self.location.href='ListMoreport.php?melve=MoreportReDtPageAll&start=0".$addcs.hReturnElveHashStrHref(0)."';</script>";
		exit();
	}
	echo"<meta http-equiv=\"refresh\" content=\"".$public_r['realltime'].";url=ListMoreport.php?melve=MoreportUpdateClassfileAll&start=$new_start".$addcs.hReturnElveHashStrHref(0)."\">".$fun_r[OneMoreportUpdateClassfileSuccess]."(ID:<font color=red><b>".$new_start."</b></font>)";
	exit();
}

//更新动态页面
function Moreport_ReDtPageAll($add,$userid,$username){
	global $elves,$dbtbpre,$public_r,$elve_config,$fun_r;
	$addcs=Moreport_ChangeAddCs($add);
	if(!$add['dodtpage'])
	{
		echo"<script>self.location.href='ListMoreport.php?melve=MoreportClearTmpfileAll&start=0".$addcs.hReturnElveHashStrHref(0)."';</script>";
		exit();
	}
	$start=(int)$add['start'];
	$num=1;
	$b=0;
	$sql=$elves->query("select * from {$dbtbpre}melvemoreport where pid>$start order by pid limit ".$num);
	while($r=$elves->fetch($sql))
	{
		$b=1;
		$new_start=$r['pid'];
		if($r['pid']==1)
		{
			continue;
		}
		if(!$r['ppath']||!file_exists($r['ppath'].'core/config/config.php'))
		{
			continue;
		}
		define('ELVE_SELFPATH',$r['ppath']);
		Moreport_ChangeData($r,1);
	}
	if(empty($b))
	{
		echo $fun_r[MoreportReDtPageSuccess]."<script>self.location.href='ListMoreport.php?melve=MoreportClearTmpfileAll&start=0".$addcs.hReturnElveHashStrHref(0)."';</script>";
		exit();
	}
	echo"<meta http-equiv=\"refresh\" content=\"".$public_r['realltime'].";url=ListMoreport.php?melve=MoreportReDtPageAll&start=$new_start".$addcs.hReturnElveHashStrHref(0)."\">".$fun_r[OneMoreportReDtPageSuccess]."(ID:<font color=red><b>".$new_start."</b></font>)";
	exit();
}

//清理临时文件
function Moreport_ClearTmpfileAll($add,$userid,$username){
	global $elves,$dbtbpre,$public_r,$elve_config,$fun_r;
	$addcs=Moreport_ChangeAddCs($add);
	if(!$add['dotmpfile'])
	{
		echo "<script>self.location.href='ListMoreport.php?melve=MoreportReIndexfileAll&start=0".$addcs.hReturnElveHashStrHref(0)."';</script>";
		exit();
	}
	$start=(int)$add['start'];
	$num=1;
	$b=0;
	$sql=$elves->query("select * from {$dbtbpre}melvemoreport where pid>$start order by pid limit ".$num);
	while($r=$elves->fetch($sql))
	{
		$b=1;
		$new_start=$r['pid'];
		if($r['pid']==1)
		{
			continue;
		}
		if(!$r['ppath']||!file_exists($r['ppath'].'core/config/config.php'))
		{
			continue;
		}
		define('ELVE_SELFPATH',$r['ppath']);
		Moreport_ChangeData($r,2);
	}
	if(empty($b))
	{
		echo $fun_r[MoreportClearTmpfileSuccess]."<script>self.location.href='ListMoreport.php?melve=MoreportReIndexfileAll&start=0".$addcs.hReturnElveHashStrHref(0)."';</script>";
		exit();
	}
	echo"<meta http-equiv=\"refresh\" content=\"".$public_r['realltime'].";url=ListMoreport.php?melve=MoreportClearTmpfileAll&start=$new_start".$addcs.hReturnElveHashStrHref(0)."\">".$fun_r[OneMoreportClearTmpfileSuccess]."(ID:<font color=red><b>".$new_start."</b></font>)";
	exit();
}

//更新首页文件
function Moreport_ReIndexfileAll($add,$userid,$username){
	global $elves,$dbtbpre,$public_r,$elve_config,$fun_r;
	$addcs=Moreport_ChangeAddCs($add);
	if(!$add['doreindex'])
	{
		insert_dolog("");//操作日志
		printerror("MoreportChangeAllDataSuccess","ListMoreport.php".hReturnElveHashStrHref2(1));
	}
	$start=(int)$add['start'];
	$num=1;
	$b=0;
	$sql=$elves->query("select * from {$dbtbpre}melvemoreport where pid>$start order by pid limit ".$num);
	while($r=$elves->fetch($sql))
	{
		$b=1;
		$new_start=$r['pid'];
		if($r['pid']==1)
		{
			continue;
		}
		if(!$r['ppath']||!file_exists($r['ppath'].'core/config/config.php'))
		{
			continue;
		}
		define('ELVE_SELFPATH',$r['ppath']);
		Moreport_ChangeData($r,4);
	}
	if(empty($b))
	{
		insert_dolog("");//操作日志
		printerror("MoreportChangeAllDataSuccess","ListMoreport.php".hReturnElveHashStrHref2(1));
		//echo $fun_r[MoreportReIndexfileSuccess]."<script>self.location.href='ListMoreport.php?melve=MoreportReIndexfileAll&start=0".$addcs.hReturnElveHashStrHref(0)."';</script>";
		exit();
	}
	echo"<meta http-equiv=\"refresh\" content=\"".$public_r['realltime'].";url=ListMoreport.php?melve=MoreportReIndexfileAll&start=$new_start".$addcs.hReturnElveHashStrHref(0)."\">".$fun_r[OneMoreportReIndexfileSuccess]."(ID:<font color=red><b>".$new_start."</b></font>)";
	exit();
}

//附加参数
function Moreport_ChangeAddCs($add){
	$docache=(int)$add['docache'];
	$doclassfile=(int)$add['doclassfile'];
	$dodtpage=(int)$add['dodtpage'];
	$dotmpfile=(int)$add['dotmpfile'];
	$doreindex=(int)$add['doreindex'];
	$cs='';
	if($docache)
	{
		$cs.="&docache=".$docache;
	}
	if($doclassfile)
	{
		$cs.="&doclassfile=".$doclassfile;
	}
	if($dodtpage)
	{
		$cs.="&dodtpage=".$dodtpage;
	}
	if($dotmpfile)
	{
		$cs.="&dotmpfile=".$dotmpfile;
	}
	if($doreindex)
	{
		$cs.="&doreindex=".$doreindex;
	}
	return $cs;
}

//更新数据
function Moreport_ChangeData($portr,$elve=0){
	global $elves,$dbtbpre,$public_r,$elve_config;
	$elve_config['sets']['deftempid']=$portr['tempgid'];
	if($elve==1)//更新动态页面
	{
		GetPlTempPage();//评论列表模板
		GetPlJsPage();//评论JS模板
		ReCptemp();//控制面板模板
		GetSearch();//三搜索表单模板
		GetPrintPage();//打印模板
		GetDownloadPage();//下载地址页面
		ReGbooktemp();//留言板模板
		ReLoginIframe();//登陆状态模板
		ReSchAlltemp();//全站搜索模板
		//防采集缓存
		$yfile=ELVE_PATH.'core/data/dbcache/notcj.php';
		$nfile=ELVE_SELFPATH.'core/data/dbcache/notcj.php';
		@copy($yfile,$nfile);
	}
	elseif($elve==2)//清理临时文件
	{
		//临时文件目录
		$tmppath=ELVE_SELFPATH.'core/data/tmp';
		$hand=@opendir($tmppath);
		while($file=@readdir($hand))
		{
			if($file=='.'||$file=='..'||$file=='test.txt'||$file=='index.html'||$file=='mod'||$file=='temp'||$file=='titlepic'||$file=='cj')
			{
				continue;
			}
			$filename=$tmppath.'/'.$file;
			if(!is_dir($filename))
			{
				DelFiletext($filename);
			}
		}
	}
	elseif($elve==3)//更新栏目缓存文件
	{
		$ypath=ELVE_PATH.'d/js';
		$npath=ELVE_SELFPATH.'d/js';
		CopyPath($ypath,$npath);
		$ypath=ELVE_PATH.'core/data/fc';
		$npath=ELVE_SELFPATH.'core/data/fc';
		CopyPath($ypath,$npath);
		$ypath=ELVE_PATH.'core/data/html';
		$npath=ELVE_SELFPATH.'core/data/html';
		CopyPath($ypath,$npath);
		$ypath=ELVE_PATH.'core/data/template';
		$npath=ELVE_SELFPATH.'core/data/template';
		CopyPath($ypath,$npath);
	}
	elseif($elve==4)//更新动态首页
	{
		if($portr['mustdt']||$public_r['indexpagedt'])
		{
			DelFiletext(ELVE_SELFPATH.'index'.$public_r['indextype']);
			@copy(ELVE_SELFPATH.'core/data/template/dtindexpage.txt',ELVE_SELFPATH.'index.php');
		}
	}
	else//更新数据库缓存
	{
		//更新参数设置
		GetConfig(1);
		//更新类别
		GetClass();
		//更新会员组
		GetMemberLevel();
		//更新全站搜索数据表
		GetSearchAllTb();
	}
}

?>